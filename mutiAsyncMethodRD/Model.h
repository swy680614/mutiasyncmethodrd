//
//  Model.h
//  mutiAsyncMethodRD
//
//  Created by Vin Yang on 2015/8/20.
//  Copyright (c) 2015年 Vin Yang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

- (void)iosTest:(void (^)(id))success
      onFailure:(void (^)(NSError *))failure;

- (void)iosTestDelay5Sec:(void (^)(id))success
               onFailure:(void (^)(NSError *))failure;

- (void)iosTestDelay3Sec:(void (^)(id))success
               onFailure:(void (^)(NSError *))failure;

@end
