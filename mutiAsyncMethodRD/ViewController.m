//
//  ViewController.m
//  mutiAsyncMethodRD
//
//  Created by Vin Yang on 7/17/15.
//  Copyright (c) 2015 Vin Yang. All rights reserved.
//

#import "ViewController.h"
#import "Model.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Model *model = [Model new];
    
    [model iosTestDelay5Sec:^(id result) {
        NSLog(@"JSON: %@", result);
    } onFailure:^(NSError *error) {
        NSLog(@"error: %@", error);
    }];
    
    [model iosTestDelay3Sec:^(id result) {
        NSLog(@"JSON: %@", result);
    } onFailure:^(NSError *error) {
        NSLog(@"error: %@", error);
    }];
    
    [model iosTest:^(id result) {
        NSLog(@"JSON: %@", result);
    } onFailure:^(NSError *error) {
        NSLog(@"error: %@", error);
    }];
    

}

- (void)operationQueue:(void (^)(NSString *msg))success {
    NSOperationQueue *q = [[NSOperationQueue alloc] init];
    //if need serial set maxConcurrentOperationCount = 1
    //else no need to set
//    q.maxConcurrentOperationCount = 1;
    NSOperation *operation1 = [NSBlockOperation blockOperationWithBlock:^{
        [self blockMethod1:^{
//            [NSThread sleepForTimeInterval:3];
            NSLog(@"%@",[NSThread currentThread]);
        }];
    }];
    
    NSOperation *operation2 = [NSBlockOperation blockOperationWithBlock:^{
        [self blockMethod2:^{
            NSLog(@"%@",[NSThread currentThread]);
            [q cancelAllOperations];
            success(@"operation2 success");
        }];
    }];
    
    NSOperation *opDone = [NSBlockOperation blockOperationWithBlock:^{
        NSLog(@"done");
        NSLog(@"%@",[NSThread currentThread]);
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Main thread work (UI usually)
            NSLog(@"back main thread");
            NSLog(@"%@",[NSThread currentThread]);
            success(@"all done");
        }];
    }];
    [opDone addDependency:operation1];
//    [opDone addDependency:operation2];
    [q addOperation:operation1];
//    [q addOperation:operation2];
    [q addOperation:opDone];
}

- (void)amaryllo_test:(void (^)())success {
    dispatch_queue_t queue=dispatch_queue_create("Amaryllo.eu2", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [self setAmarylloCloudEnable:^{
            NSLog(@"vin-setAmarylloCloudEnable");
        }];
    });
    dispatch_async(queue, ^{
        dispatch_group_t settingGroup = dispatch_group_create();
        NSInteger taskCount = 0;
        if (YES) {
            taskCount ++;
            dispatch_group_enter(settingGroup);
            [self turnRecAndGetRecInfo:^{
                dispatch_group_leave(settingGroup);
                NSLog(@"vin-turnRecAndGetRecInfo");
            }];
        }
        
        if (YES) {
            taskCount ++;
            dispatch_group_enter(settingGroup);
            [self setGduMode:^{
                NSLog(@"vin-setGduMode");
            }];
        }
        
        if (taskCount == 0) {
            success();
        } else {
            dispatch_group_notify(settingGroup, dispatch_get_main_queue(), ^{
                success();
            });
        }
    });
}

//will execute method concurrent
- (void)dispatch_group_async_test {
    dispatch_group_t group = dispatch_group_create();
    
    dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        [self blockMethod1:^{
            
        }];
    });
    
    
    dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        [self blockMethod2:^{
            
        }];
    });
    
    dispatch_group_notify(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
        // block3
        NSLog(@"all done");
    });
}

// will by order execute method
- (void)dispatch_group_test {
    dispatch_group_t settingGroup = dispatch_group_create();
    
    dispatch_group_enter(settingGroup);
    [self blockMethod1:^{
        NSLog(@"1");
        dispatch_group_leave(settingGroup);
    }];
    
    dispatch_group_enter(settingGroup);
    [self blockMethod2:^{
        NSLog(@"2");
        dispatch_group_leave(settingGroup);
    }];
    
    dispatch_group_notify(settingGroup, dispatch_get_main_queue(), ^{
        NSLog(@"done");
    });
}

- (void)setAmarylloCloudEnable:(void (^)())success {
    [NSThread sleepForTimeInterval:9];
    success();
}

- (void)turnRecAndGetRecInfo:(void (^)())success {
    dispatch_queue_t queue=dispatch_queue_create("Amaryllo.eu1", DISPATCH_QUEUE_SERIAL);
    dispatch_sync(queue, ^{
        [self setRecEnabled:^{
            [NSThread sleepForTimeInterval:3];
            NSLog(@"vin-setRecEnabled");
        }];
    });
    dispatch_sync(queue, ^{
        [self getRecOnSuccess:^{
            NSLog(@"vin-getRecOnSuccess");
            success();
        }];
    });
}

- (void)setGduMode:(void (^)())success {
    success();
}

- (void)setRecEnabled:(void (^)())success {
    [NSThread sleepForTimeInterval:6];
    success();
}

- (void)getRecOnSuccess:(void (^)())success {
    [NSThread sleepForTimeInterval:1];
    success();
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)blockMethod1:(void (^)())success {
//    NSOperationQueue *q = [[NSOperationQueue alloc] init];
//    q.maxConcurrentOperationCount = 1;
//    NSBlockOperation *op = [NSBlockOperation new];
//    [NSThread sleepForTimeInterval:5];
//    [op addExecutionBlock:^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            NSLog(@"blockMethod1 done");
//            success();
//        });
//    }];
//    [q addOperation:op];
//
    [NSThread sleepForTimeInterval:6];
    NSLog(@"method1");
    success();
}

- (void)blockMethod2:(void (^)())success {
    NSLog(@"method2");
    success();
}

- (void)gcd_barrier_test {
    [NSThread currentThread].name=@"mainThread";
    
    dispatch_queue_t queue=dispatch_queue_create("RICHARD", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        
        [NSThread currentThread].name=@"blk0";
        
        [NSThread sleepForTimeInterval:4];
        
        NSLog(@"blk0");});
    
    dispatch_async(queue,  ^{
        
        [NSThread currentThread].name=@"blk1";
        
        [NSThread sleepForTimeInterval:2];
        
        NSLog(@"blk1");});
    
    dispatch_barrier_async(queue, ^{NSLog(@"dispatch_barrier");
        
        [NSThread sleepForTimeInterval:5];
        
    });
    
    dispatch_async(queue, ^{
        
        [NSThread currentThread].name=@"blk2";
        
        [NSThread sleepForTimeInterval:3];
        
        NSLog(@"blk2");});
    
    NSLog(@"hello");
}

@end
