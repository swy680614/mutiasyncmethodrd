//
//  Model.m
//  mutiAsyncMethodRD
//
//  Created by Vin Yang on 2015/8/20.
//  Copyright (c) 2015年 Vin Yang. All rights reserved.
//

#import "Model.h"
#import "AFNetworking.h"

@implementation Model


- (void)iosTest:(void (^)(id))success
      onFailure:(void (^)(NSError *))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://122.116.232.79:9001/api/iOSTest/" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)iosTestDelay5Sec:(void (^)(id))success
      onFailure:(void (^)(NSError *))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://122.116.232.79:9001/api/iosTestDelay5Sec/" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

- (void)iosTestDelay3Sec:(void (^)(id))success
               onFailure:(void (^)(NSError *))failure {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://122.116.232.79:9001/api/iosTestDelay3Sec/" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

@end
